package authorize

import (
	"crypto/rsa"
	"io/ioutil"
	"os"

	"github.com/golang-jwt/jwt"
)

type signerConfig struct {
	filename   string
	privateKey *rsa.PrivateKey
}

type signerOption func(*signerConfig)

type Signer interface {
	Sign(token *jwt.Token) (string, error)
	KeyFunc(token *jwt.Token) (any, error)
}

type rsaSigner struct {
	privateKey *rsa.PrivateKey
}

func (s *rsaSigner) Sign(token *jwt.Token) (string, error) {
	return token.SignedString(s.privateKey)
}

func (s *rsaSigner) KeyFunc(token *jwt.Token) (any, error) {
	if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
		return nil, InvalidTokenError
	}

	return &s.privateKey.PublicKey, nil
}

func NewRSASigner(opts ...signerOption) (*rsaSigner, error) {
	var config signerConfig
	for _, opt := range opts {
		opt(&config)
	}

	if config.privateKey != nil {
		return &rsaSigner{config.privateKey}, nil
	}

	if len(config.filename) == 0 {
		return nil, InvalidSignerConfiguration
	}

	f, err := os.Open(config.filename)
	if err != nil {
		return nil, err
	}

	data, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}

	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM(data)
	if err != nil {
		return nil, err
	}

	return &rsaSigner{privateKey}, nil
}

func WithKeyFilename(filename string) signerOption {
	return func(config *signerConfig) {
		config.filename = filename
	}
}

func WithPrivateKey(privateKey *rsa.PrivateKey) signerOption {
	return func(config *signerConfig) {
		config.privateKey = privateKey
	}
}
