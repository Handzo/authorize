package model

import (
	"context"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

const (
	ROOT_ROLE    = "root"
	ADMIN_ROLE   = "admin"
	MANAGER_ROLE = "manager"
	USER_ROLE    = "user"
)

type Role struct {
	ID   int    `gorm:"primaryKey"`
	Name string `gorm:"uniqueIndex"`
}

func (role *Role) Init(ctx context.Context, db *gorm.DB) error {
	roles := []Role{
		{Name: ROOT_ROLE},
		{Name: ADMIN_ROLE},
		{Name: MANAGER_ROLE},
		{Name: USER_ROLE},
	}

	return db.Clauses(clause.OnConflict{DoNothing: true}).Create(roles).Error
}
