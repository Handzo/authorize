package model

import (
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	ID             string `gorm:"type:uuid;primary_key;default:uuid_generate_v4();"`
	Email          string `gorm:"uniqueIndex"`
	HashedPassword string `gorm:"not null"`
	Blocked        bool   `gorm:"default:false"`
	Roles          []Role `gorm:"many2many:user_to_roles;"`
}

func (u User) IsValidPassword(password string) bool {
	return bcrypt.CompareHashAndPassword([]byte(u.HashedPassword), []byte(password)) == nil
}

func HashPassword(password string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	if err != nil {
		return "", err
	}

	return string(hashedPassword), nil
}

func (u User) GetRoles() []string {
	roles := make([]string, len(u.Roles))
	for i, role := range u.Roles {
		roles[i] = role.Name
	}

	return roles
}
