package model

import (
	"time"

	"gorm.io/gorm"
)

type VerificationCode struct {
	gorm.Model
	UserId    string    `gorm:"not null"`
	Used      bool      `gorm:"not null,default:false"`
	ExpiredAt time.Time `bun:"not null"`
}
