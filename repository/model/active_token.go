package model

type ActiveToken struct {
	UserId    string `gorm:"primaryKey"`
	TokenType string `gorm:"primaryKey"`
	TokenId   string `gorm:"not null"`
}
