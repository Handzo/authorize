package repository

import (
	"context"

	"gitlab.com/Handzo/authorize/repository/model"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type Repository interface {
	Begin() Repository
	Commit() Repository
	Rollback() Repository

	Create(ctx context.Context, m any) error
	Update(ctx context.Context, m any) error

	FindUserById(ctx context.Context, userId string) (*model.User, error)
	FindUserByEmail(ctx context.Context, email string) (*model.User, error)
	UpsertActiveToken(ctx context.Context, activeToken *model.ActiveToken) error
	FindActiveToken(ctx context.Context, userId, tokenType string) (*model.ActiveToken, error)
	RevokeTokenByUserId(ctx context.Context, userid string) error
	FindVerificationCode(ctx context.Context, verificationCodeId string) (*model.VerificationCode, error)
	SaveVerificationCode(ctx context.Context, verificationCode *model.VerificationCode) error
}

func New(db *gorm.DB) *repo {
	return &repo{
		db: db,
	}
}

type repo struct {
	db *gorm.DB
}

func (b *repo) Begin() Repository {
	return &repo{
		db: b.db.Begin(),
	}
}
func (b *repo) Commit() Repository {
	return &repo{
		db: b.db.Commit(),
	}
}
func (b *repo) Rollback() Repository {
	return &repo{
		db: b.db.Rollback(),
	}
}

func (r repo) Create(ctx context.Context, m any) error {
	return r.db.Create(m).Error
}

func (r repo) Update(ctx context.Context, m any) error {
	return r.db.Save(m).Error
}

func (r repo) Init() error {
	return r.db.AutoMigrate(
		(*model.Role)(nil),
		(*model.User)(nil),
		(*model.ActiveToken)(nil),
		(*model.VerificationCode)(nil),
	)
}

func (r repo) DropTables() error {
	return r.db.Migrator().DropTable(
		(*model.Role)(nil),
		(*model.User)(nil),
		(*model.ActiveToken)(nil),
		(*model.VerificationCode)(nil),
	)
}

func (r repo) FindUserById(ctx context.Context, userId string) (*model.User, error) {
	user := &model.User{}
	result := r.db.Where(`id = ?`, userId).First(user)
	return user, result.Error
}

func (r repo) FindUserByEmail(ctx context.Context, email string) (*model.User, error) {
	user := &model.User{}

	result := r.db.Where("email = ?", email).First(user)

	if result.Error != nil && result.Error == gorm.ErrRecordNotFound {
		return nil, UserNotFoundError
	}

	return user, result.Error
}

func (r repo) UpsertActiveToken(ctx context.Context, activeToken *model.ActiveToken) error {
	return r.db.Clauses(clause.OnConflict{
		Columns:   []clause.Column{{Name: "user_id"}, {Name: "token_type"}},
		DoUpdates: clause.AssignmentColumns([]string{"token_id"}),
	}).Create(activeToken).Error
}

func (r repo) FindActiveToken(ctx context.Context, userId, tokenType string) (*model.ActiveToken, error) {
	activeToken := &model.ActiveToken{}

	result := r.db.Where("user_id = ? AND token_type = ?", userId, tokenType).First(activeToken)

	return activeToken, result.Error
}

func (r repo) RevokeTokenByUserId(ctx context.Context, userId string) error {
	return r.db.Delete(&model.User{}, userId).Error
}

func (r repo) FindVerificationCode(ctx context.Context, verificationCodeId string) (*model.VerificationCode, error) {
	verificationCode := &model.VerificationCode{}

	result := r.db.First(verificationCode, verificationCodeId)

	return verificationCode, result.Error
}

func (r repo) SaveVerificationCode(ctx context.Context, verificationCode *model.VerificationCode) error {
	return r.db.Create(verificationCode).Error
}
