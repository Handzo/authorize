package repository

type RepositoryError int

const (
	UserNotFoundError RepositoryError = iota
)

var errStringMap = map[RepositoryError]string{
	UserNotFoundError: "user_not_found",
}

func (e RepositoryError) Error() string {
	if msg, ok := errStringMap[e]; ok {
		return msg
	}

	return "unknown error"
}
