package repository_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/Handzo/authorize/repository"
	"gitlab.com/Handzo/authorize/repository/model"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func createRepository(t *testing.T) repository.Repository {
	db, err := gorm.Open(sqlite.Open("test.db"), &gorm.Config{})
	require.NoError(t, err, "open database error")

	repo := repository.New(db)

	err = repo.DropTables()
	require.NoError(t, err, "drop tables error")

	err = repo.Init()
	require.NoError(t, err, "migration error")

	return repo
}

func TestCreateUser(t *testing.T) {
	r := require.New(t)

	repo := createRepository(t)

	email := "handzo@gmail.com"
	user := &model.User{
		Email:          email,
		HashedPassword: "asdasd",
		Roles:          []model.Role{model.Role{Name: "user"}},
	}

	err := repo.Create(context.Background(), user)

	fmt.Printf("%+v\n", user)
	r.NoError(err, "create user error")
}
