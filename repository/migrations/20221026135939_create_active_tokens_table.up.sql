CREATE TABLE IF NOT EXISTS active_tokens (
    user_id uuid NOT NULL,
    token_type VARCHAR(20) NOT NULL,
    token_id uuid NOT NULL,
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    PRIMARY KEY(user_id, token_type),
    CONSTRAINT fk_active_token_user_id_token_type
        FOREIGN KEY(user_id)
            REFERENCES users(id)
);
