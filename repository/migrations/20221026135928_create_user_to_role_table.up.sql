CREATE TABLE IF NOT EXISTS user_to_roles (
    user_id uuid PRIMARY KEY,
    role_id INT NOT NULL,
    CONSTRAINT fk_user_roles_user_id
        FOREIGN KEY(user_id)
            REFERENCES users(id),
    CONSTRAINT fk_user_roles_role_id
        FOREIGN KEY(role_id)
            REFERENCES roles(id)
);
