package authorize

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	InvalidTokenError              = status.Error(codes.Unauthenticated, "invalidToken")
	InvalidSignerConfiguration     = status.Error(codes.Internal, "invalid signer configuration")
	InvalidUsernameOrPasswordError = status.Error(codes.InvalidArgument, "invalid email or password")
	InvalidPasswordError           = status.Error(codes.InvalidArgument, "invalid password")
	VerificationCodeExpiredError   = status.Error(codes.InvalidArgument, "verification code expired")
	InvalidVerificationCodeError   = status.Error(codes.InvalidArgument, "invalid verification code")
)
