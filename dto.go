package authorize

import (
	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
)

type Tokens struct {
	AccessToken  string `json:"access_token"`
	AtExpires    int64  `json:"access_token_expires_at"`
	RefreshToken string `json:"refresh_token"`
	RtExpires    int64  `json:"refresh_token_expires_at"`
}

type Creds struct {
	UserId  string
	Blocked bool
}

type Claims struct {
	jwt.StandardClaims
	UserId    string `json:"user_id"`
	TokenType string `json:"token_type"`
}

type User struct {
	Id      string   `json:"id"`
	Email   string   `json:"email"`
	Blocked bool     `json:"blocked"`
	Roles   []string `json:"roles"`
}

func NewClaims(userId string, expiresAt int64, tokenType string) Claims {
	return Claims{
		StandardClaims: jwt.StandardClaims{
			Id:        uuid.NewString(),
			ExpiresAt: expiresAt,
		},
		UserId:    userId,
		TokenType: tokenType,
	}
}
