package authorize

import (
	"context"
	"time"

	"github.com/golang-jwt/jwt"
	"gitlab.com/Handzo/authorize/repository"
	"gitlab.com/Handzo/authorize/repository/model"
	"gitlab.com/Handzo/gore/log"
)

type AuthService interface {
	Register(ctx context.Context, email, password string) (*Tokens, error)
	Login(ctx context.Context, email, password string) (*Tokens, error)
	Logout(ctx context.Context, accessToken string) error
	ChangePassword(ctx context.Context, userId, oldPassword, newPassword string) error
	RestorePassword(ctx context.Context, codeId, newPassword string) (*Tokens, error)
	RefreshToken(ctx context.Context, refreshToken string) (*Tokens, error)
	VerifyToken(ctx context.Context, tokenStr string) (*User, error)
	SendVerificationCode(ctx context.Context, email string) error
}

type authService struct {
	logger log.Logger
	config Config
	signer Signer
	repo   repository.Repository
}

func New(logger log.Logger, config Config, signer Signer, repo repository.Repository) *authService {
	return &authService{
		logger: logger,
		config: config,
		signer: signer,
		repo:   repo,
	}
}

func (a *authService) Register(ctx context.Context, email, password string) (*Tokens, error) {
	repo := a.repo.Begin()
	defer repo.Rollback()

	hashedPassword, err := model.HashPassword(password)

	role := model.Role{
		ID:   1,
		Name: model.USER_ROLE,
	}

	user := &model.User{
		Email:          email,
		HashedPassword: hashedPassword,
		Roles:          []model.Role{role},
	}

	if err := repo.Create(ctx, user); err != nil {
		return nil, err
	}

	tokens, err := a.createTokens(ctx, repo, user)
	if err != nil {
		return nil, err
	}

	repo.Commit()
	return tokens, nil
}

func (a *authService) Login(ctx context.Context, email, password string) (*Tokens, error) {
	repo := a.repo.Begin()
	defer repo.Rollback()

	user, err := repo.FindUserByEmail(ctx, email)
	if err != nil {
		return nil, err
	}

	if !user.IsValidPassword(password) {
		return nil, InvalidUsernameOrPasswordError
	}

	tokens, err := a.createTokens(ctx, repo, user)
	if err != nil {
		return nil, err
	}

	repo.Commit()
	return tokens, nil
}

func (a *authService) Logout(ctx context.Context, accessToken string) error {
	repo := a.repo.Begin()
	defer repo.Rollback()

	user, _, err := a.verifyToken(ctx, repo, accessToken)
	if err != nil {
		return err
	}

	if err = repo.RevokeTokenByUserId(ctx, user.ID); err != nil {
		return err
	}

	repo.Commit()
	return nil
}

func (a *authService) ChangePassword(ctx context.Context, userId, oldPassword, newPassword string) error {
	repo := a.repo.Begin()
	defer repo.Rollback()

	user, err := repo.FindUserById(ctx, userId)
	if err != nil {
		return err
	}

	if !user.IsValidPassword(oldPassword) {
		return InvalidPasswordError
	}

	user.HashedPassword, err = model.HashPassword(newPassword)
	if err != nil {
		return err
	}

	if err = repo.Update(ctx, user); err != nil {
		return err
	}

	repo.Commit()
	return nil
}

func (a *authService) RestorePassword(ctx context.Context, codeId, newPassword string) (*Tokens, error) {
	repo := a.repo.Begin()
	defer repo.Rollback()

	verificationCode, err := repo.FindVerificationCode(ctx, codeId)
	if err != nil {
		return nil, InvalidVerificationCodeError
	}

	if verificationCode.Used {
		return nil, InvalidVerificationCodeError
	}

	if time.Now().After(verificationCode.ExpiredAt) {
		return nil, VerificationCodeExpiredError
	}

	user, err := repo.FindUserById(ctx, verificationCode.UserId)
	if err != nil {
		return nil, err
	}

	user.HashedPassword, err = model.HashPassword(newPassword)
	if err != nil {
		return nil, err
	}

	if err = repo.Update(ctx, user); err != nil {
		return nil, err
	}

	verificationCode.Used = true

	if err = repo.SaveVerificationCode(ctx, verificationCode); err != nil {
		return nil, err
	}

	tokens, err := a.createTokens(ctx, repo, user)
	if err != nil {
		return nil, err
	}

	repo.Commit()
	return tokens, nil
}

func (a *authService) RefreshToken(ctx context.Context, refreshToken string) (*Tokens, error) {
	repo := a.repo.Begin()
	defer repo.Rollback()

	user, activeToken, err := a.verifyToken(ctx, repo, refreshToken)
	if err != nil {
		return nil, err
	}

	if activeToken.TokenType != "refresh_token" {
		return nil, InvalidTokenError
	}

	tokens, err := a.createTokens(ctx, repo, user)
	if err != nil {
		return nil, err
	}

	repo.Commit()
	return tokens, nil
}

func (a *authService) VerifyToken(ctx context.Context, tokenStr string) (*User, error) {
	repo := a.repo.Begin()
	defer repo.Rollback()

	user, _, err := a.verifyToken(ctx, repo, tokenStr)
	if err != nil {
		return nil, err
	}

	roles := make([]string, len(user.Roles))
	for i, role := range user.Roles {
		roles[i] = role.Name
	}

	repo.Commit()
	return &User{
		Id:      user.ID,
		Email:   user.Email,
		Blocked: user.Blocked,
		Roles:   roles,
	}, nil
}

func (a *authService) verifyToken(ctx context.Context, repo repository.Repository, tokenStr string) (*model.User, *model.ActiveToken, error) {
	a.logger.Info("start verification")
	token, err := a.parseToken(tokenStr)
	if err != nil {
		a.logger.Warnf("verification: parse token error: %v\n", err)
		return nil, nil, InvalidTokenError
	}

	claims, ok := token.Claims.(*Claims)
	if !ok || !token.Valid {
		a.logger.Warn("verification: cast claims error")
		return nil, nil, InvalidTokenError
	}

	if err := claims.Valid(); err != nil {
		a.logger.Warnf("verification: invalid claims error: %v\n", err)
		return nil, nil, InvalidTokenError
	}

	activeToken, err := repo.FindActiveToken(ctx, claims.UserId, claims.TokenType)
	if err != nil {
		a.logger.Warnf("verification: cannot find active token: %v\n", err)
		return nil, nil, InvalidTokenError
	}

	user, err := repo.FindUserById(ctx, claims.UserId)
	if err != nil {
		a.logger.Warnf("verification: cannot find user: %v\n", err)
		return nil, nil, InvalidTokenError
	}

	return user, activeToken, nil
}

func (a *authService) SendVerificationCode(ctx context.Context, email string) error {
	repo := a.repo.Begin()
	defer repo.Rollback()

	user, err := repo.FindUserByEmail(ctx, email)
	if err != nil {
		return err
	}

	now := time.Now()
	verificationCode := &model.VerificationCode{
		UserId:    user.ID,
		ExpiredAt: now.Add(a.config.VerificationCodeExpirationTime),
	}

	if err = repo.SaveVerificationCode(ctx, verificationCode); err != nil {
		return err
	}

	// TODO: send verification code

	repo.Commit()
	return nil
}

func (a *authService) createTokens(ctx context.Context, repo repository.Repository, user *model.User) (*Tokens, error) {
	now := time.Now()

	atExpiresAt := now.Add(a.config.AccessTokenExpirationTime).Unix()
	rtExpiresAt := now.Add(a.config.RefreshTokenExpirationTime).Unix()

	atClaims := NewClaims(user.ID, atExpiresAt, "access_token")

	accessToken, err := a.createToken(atClaims)
	if err != nil {
		return nil, err
	}

	rtClaims := NewClaims(user.ID, rtExpiresAt, "refresh_token")

	refreshToken, err := a.createToken(rtClaims)
	if err != nil {
		return nil, err
	}

	accessActiveToken := &model.ActiveToken{
		UserId:    user.ID,
		TokenId:   atClaims.Id,
		TokenType: atClaims.TokenType,
	}

	refreshActiveToken := &model.ActiveToken{
		UserId:    user.ID,
		TokenId:   rtClaims.Id,
		TokenType: rtClaims.TokenType,
	}

	if err := repo.UpsertActiveToken(ctx, accessActiveToken); err != nil {
		return nil, err
	}

	if err := repo.UpsertActiveToken(ctx, refreshActiveToken); err != nil {
		return nil, err
	}

	return &Tokens{
		AccessToken:  accessToken,
		AtExpires:    atExpiresAt,
		RefreshToken: refreshToken,
		RtExpires:    rtExpiresAt,
	}, nil
}

func (a *authService) createToken(claims Claims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)

	return a.signer.Sign(token)
}

func (a *authService) parseToken(tokenStr string) (*jwt.Token, error) {
	return jwt.ParseWithClaims(tokenStr, &Claims{}, a.signer.KeyFunc)
}
