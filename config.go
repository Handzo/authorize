package authorize

import (
	"time"

	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	Port                           string        `default:"8080"`
	AccessTokenExpirationTime      time.Duration `default:"5m" split_words:"true"`
	RefreshTokenExpirationTime     time.Duration `default:"168h" split_words:"true"`
	VerificationCodeExpirationTime time.Duration `default:"5m" split_words:"true"`

	RSAKeyFilename string `envconfig:"AUTHORIZE_RSA_KEY_FILENAME" required:"true"`

	Postgres struct {
		Database     string        `envconfig:"AUTHORIZE_POSTGRES_DATABASE" default:"authorize"`
		Username     string        `envconfig:"AUTHORIZE_POSTGRES_USER" default:"authorize_user"`
		Password     string        `envconfig:"AUTHORIZE_POSTGRES_PASSWORD"`
		Host         string        `envconfig:"AUTHORIZE_POSTGRES_HOST" default:"postgres"`
		Port         string        `envconfig:"AUTHORIZE_POSTGRES_PORT" default:"5432"`
		SslMode      bool          `envconfig:"AUTHORIZE_POSTGRES_SSL" default:"true"`
		DialTimeout  time.Duration `envconfig:"AUTHORIZE_POSTGRES_DIAL_TIMEOUT" default:"5s"`
		Timeout      time.Duration `envconfig:"AUTHORIZE_POSTGRES_TIMEOUT" default:"5s"`
		ReadTimeout  time.Duration `envconfig:"AUTHORIZE_POSTGRES_READ_TIMEOUT" default:"5s"`
		WriteTimeout time.Duration `envconfig:"AUTHORIZE_POSTGRES_WRITE_TIMEOUT" default:"5s"`
	}
}

func ConfigFromEnv() (Config, error) {
	var conf Config

	return conf, envconfig.Process("authorize", &conf)
}
