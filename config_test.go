package authorize_test

import (
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/Handzo/authorize"
)

func TestGetDefaultConfigFromEnv(t *testing.T) {
	config, err := authorize.ConfigFromEnv()

	r := require.New(t)

	r.ErrorContains(err, "required key AUTHORIZE_RSA_KEY_FILENAME missing value")

	os.Setenv("AUTHORIZE_RSA_KEY_FILENAME", "somekey.key")

	config, err = authorize.ConfigFromEnv()

	r.NoError(err, "default configuration from env error")

	r.Equal("8080", config.Port, "unexpected default port")
	r.Equal(time.Minute*5, config.AccessTokenExpirationTime, "unexpected access token default expiration time")
	r.Equal(time.Hour*168, config.RefreshTokenExpirationTime, "unexpected refresh token default expiration time")
	r.Equal(time.Minute*5, config.VerificationCodeExpirationTime, "unexpected verification code default expiration time")
	r.Equal("somekey.key", config.RSAKeyFilename, "not empty secret key filename")

	timeout := time.Second * 5
	r.Equal("authorize", config.Postgres.Database, "unexpected default database name")
	r.Equal("authorize_user", config.Postgres.Username, "unexpected default database username")
	r.Equal("", config.Postgres.Password, "unexpected default database password")
	r.Equal("postgres", config.Postgres.Host, "unexpected default database host")
	r.Equal("5432", config.Postgres.Port, "unexpected default database port")
	r.True(config.Postgres.SslMode, "unexpected default ssl mode")
	r.Equal(timeout, config.Postgres.DialTimeout, "unexpected default database dial timeout")
	r.Equal(timeout, config.Postgres.Timeout, "unexpected default database timeout")
	r.Equal(timeout, config.Postgres.ReadTimeout, "unexpected default database read timeout")
	r.Equal(timeout, config.Postgres.WriteTimeout, "unexpected default database write timeout")
}

func TestGetConfigFromEnv(t *testing.T) {
	os.Setenv("AUTHORIZE_PORT", "1234")
	os.Setenv("AUTHORIZE_ACCESS_TOKEN_EXPIRATION_TIME", "10m")
	os.Setenv("AUTHORIZE_REFRESH_TOKEN_EXPIRATION_TIME", "16h")
	os.Setenv("AUTHORIZE_VERIFICATION_CODE_EXPIRATION_TIME", "123m")
	os.Setenv("AUTHORIZE_RSA_KEY_FILENAME", "somekey.key")

	database := "mydb"
	user := "myuser"
	password := "mypassword"
	host := "myhost"
	port := "1122"
	ssl := "false"
	dialTimeout := time.Minute * 1
	timeout := time.Minute * 2
	readTimeout := time.Minute * 3
	writeTimeout := time.Minute * 4

	os.Setenv("AUTHORIZE_POSTGRES_DATABASE", database)
	os.Setenv("AUTHORIZE_POSTGRES_USER", user)
	os.Setenv("AUTHORIZE_POSTGRES_PASSWORD", password)
	os.Setenv("AUTHORIZE_POSTGRES_HOST", host)
	os.Setenv("AUTHORIZE_POSTGRES_PORT", port)
	os.Setenv("AUTHORIZE_POSTGRES_SSL", ssl)
	os.Setenv("AUTHORIZE_POSTGRES_DIAL_TIMEOUT", dialTimeout.String())
	os.Setenv("AUTHORIZE_POSTGRES_TIMEOUT", timeout.String())
	os.Setenv("AUTHORIZE_POSTGRES_READ_TIMEOUT", readTimeout.String())
	os.Setenv("AUTHORIZE_POSTGRES_WRITE_TIMEOUT", writeTimeout.String())

	config, err := authorize.ConfigFromEnv()

	r := require.New(t)

	r.NoError(err, "default configuration from env error")

	r.Equal("1234", config.Port, "unexpected default port")
	r.Equal(time.Minute*10, config.AccessTokenExpirationTime, "unexpected access token expiration time")
	r.Equal(time.Hour*16, config.RefreshTokenExpirationTime, "unexpected refresh token expiration time")
	r.Equal(time.Minute*123, config.VerificationCodeExpirationTime, "unexpected verification code expiration time")
	r.Equal("somekey.key", config.RSAKeyFilename, "unexpected secret key filename")

	r.Equal(database, config.Postgres.Database, "unexpected database name")
	r.Equal(user, config.Postgres.Username, "unexpected database username")
	r.Equal(password, config.Postgres.Password, "unexpected database password")
	r.Equal(host, config.Postgres.Host, "unexpected database host")
	r.Equal(port, config.Postgres.Port, "unexpected database port")
	r.False(config.Postgres.SslMode, "unexpected ssl mode")
	r.Equal(dialTimeout, config.Postgres.DialTimeout, "unexpected database dial timeout")
	r.Equal(timeout, config.Postgres.Timeout, "unexpected database timeout")
	r.Equal(readTimeout, config.Postgres.ReadTimeout, "unexpected database read timeout")
	r.Equal(writeTimeout, config.Postgres.WriteTimeout, "unexpected database write timeout")
}
