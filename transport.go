package authorize

import (
	"context"

	pb "gitlab.com/Handzo/authorize/proto"
)

type grpcAuthServer struct {
	pb.UnimplementedAuthServiceServer
	svc AuthService
}

func NewGRPCAuthServer(svc AuthService) *grpcAuthServer {
	return &grpcAuthServer{svc: svc}
}

func (auth *grpcAuthServer) Register(ctx context.Context, req *pb.RegisterRequest) (*pb.RegisterResponse, error) {
	tokens, err := auth.svc.Register(ctx, req.Email, req.Password)
	if err != nil {
		return nil, err
	}

	return &pb.RegisterResponse{
		AccessToken:  tokens.AccessToken,
		RefreshToken: tokens.RefreshToken,
	}, nil
}

func (auth *grpcAuthServer) Login(ctx context.Context, req *pb.LoginRequest) (*pb.LoginResponse, error) {
	tokens, err := auth.svc.Login(ctx, req.Email, req.Password)
	if err != nil {
		return nil, err
	}

	return &pb.LoginResponse{
		AccessToken:  tokens.AccessToken,
		RefreshToken: tokens.RefreshToken,
	}, nil
}

func (auth *grpcAuthServer) Logout(ctx context.Context, req *pb.LogoutRequest) (*pb.LogoutResponse, error) {
	if err := auth.svc.Logout(ctx, req.AccessToken); err != nil {
		return nil, err
	}

	return &pb.LogoutResponse{}, nil
}

func (auth *grpcAuthServer) ChangePassword(ctx context.Context, req *pb.ChangePasswordRequest) (*pb.ChangePasswordResponse, error) {
	if err := auth.svc.ChangePassword(ctx, req.UserId, req.OldPassword, req.NewPassword); err != nil {
		return nil, err
	}

	return &pb.ChangePasswordResponse{}, nil
}

func (auth *grpcAuthServer) RestorePassword(ctx context.Context, req *pb.RestorePasswordRequest) (*pb.RestorePasswordResponse, error) {
	tokens, err := auth.svc.RestorePassword(ctx, req.Code, req.Password)
	if err != nil {
		return nil, err
	}

	return &pb.RestorePasswordResponse{
		AccessToken:  tokens.AccessToken,
		RefreshToken: tokens.RefreshToken,
	}, nil
}

func (auth *grpcAuthServer) RefreshToken(ctx context.Context, req *pb.RefreshTokenRequest) (*pb.RefreshTokenResponse, error) {
	tokens, err := auth.svc.RefreshToken(ctx, req.RefreshToken)
	if err != nil {
		return nil, err
	}

	return &pb.RefreshTokenResponse{
		AccessToken:  tokens.AccessToken,
		RefreshToken: tokens.RefreshToken,
	}, nil
}

func (auth *grpcAuthServer) VerifyToken(ctx context.Context, req *pb.VerifyTokenRequest) (*pb.VerifyTokenResponse, error) {
	user, err := auth.svc.VerifyToken(ctx, req.Token)
	if err != nil {
		return nil, err
	}

	return &pb.VerifyTokenResponse{
		UserId:  user.Id,
		Roles:   user.Roles,
		Blocked: user.Blocked,
	}, nil
}

func (auth *grpcAuthServer) SendVerificationCode(ctx context.Context, req *pb.SendVerificationCodeRequest) (*pb.SendVerificationCodeResponse, error) {
	if err := auth.svc.SendVerificationCode(ctx, req.Email); err != nil {
		return nil, err
	}

	return &pb.SendVerificationCodeResponse{}, nil
}
