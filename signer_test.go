package authorize_test

import (
	"crypto/rand"
	"crypto/rsa"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/Handzo/authorize"
)

var privateKey *rsa.PrivateKey

func init() {
	var err error

	privateKey, err = rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		panic(err)
	}
}

func TestRSASigner(t *testing.T) {
	r := require.New(t)

	_, err := authorize.NewRSASigner()

	r.ErrorIs(err, authorize.InvalidSignerConfiguration, "invalid configuration")

	_, err = authorize.NewRSASigner(authorize.WithPrivateKey(privateKey))

	r.NoError(err, "signer creation error")
}
